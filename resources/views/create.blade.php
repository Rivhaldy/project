@extends('layouts.app')
    
@section('content')
    <h2>Tambah Data</h2>
    <div>
        <form action="/posts" method="POST">
            @csrf
            <div class="form-group">
                <label for="Nama">Nama Lengkap</label>
                <input type="text" class="form-control" name="Nama" value="{{old('Nama','')}}" id="Nama" placeholder="Masukkan Nama">
                @error('Nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="a">A</label>
                <input type="text" class="form-control" name="a"  value="{{old('a','')}}"  id="a" placeholder="Masukkan a">
                @error('a')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="b">B</label>
                <input type="text" class="form-control" name="b"  value="{{old('b','')}}"  id="b" placeholder="Masukkan Body">
                @error('b')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="c">C</label>
                <input type="text" class="form-control" name="c"  value="{{old('c','')}}"  id="c" placeholder="Masukkan Body">
                @error('c')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
<br>
        <div class="form-group">
            <h2>Hasil Data</h2>
        </div>
 <form method="post" action="">
            <table class="table table-bordered" style="margin-bottom: 10px" style="width:100%">
                <tr>
                    <!-- <th style="width: 10px;"><input type="checkbox" name="selectall" /></th> -->
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Body</th>
                    <th>Body</th>
                    <th>Action</th>
                
    </div>
@endsection