<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
   public function __consturct()
    {
    	$this->middleware('auth')->only(['create','update','store','edit']);
    }
        public function index()
    {
        return view('tampilan');
    }

    public function login()
    {
    	return view('auth/login');
    }
    public function register()
    {
    	return view('auth/register');
    }

    public function create()
    {
    	return view('create');
    }

   public function store(Request $request)
     {
     	$request->validate([
     		'Nama' => 'required|unique:post',
     		'a'	  => 'required',
     		'b'	  => 'required',
     		'c'   => 'required',
     	]);
     	$query = DB::table('latihanproject') ([
     		"Nama" => $request["Nama"],
     		"a"   => $request["a"],
     		"b"	  => $request["b"],
     		"c"	  => $request["c"]
     	]);

     	return redirect('/posts')->with("succes", "Data berhasil ditambahkan");
     }
}
/*     public function index()
     {
     	$projectakhir = DB::table('projectakhir')->get();
     	return view('index', compact(''));
     }

     public function index()
     {
     	$post = DB::table('projectakhir')->get();
     	return view('');
     }

      public function show($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('post.edit', compact('post'));
    }


}