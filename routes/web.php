<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::resource('','')->middleware('auth');
Auth::routes();

Route::get('/tampilan', 'HomeController@index')->name('tampilan');
Route::get('/login', 'HomeController@login')->name('login');
Route::get('/register', 'HomeController@register')->name('register');
Route::get('/create', 'HomeController@create')->name('create');

Route::post('/posts', 'HomeController@store');
Route::get('post/{post}', 'HomeController@show');
Route::get('/posts/{post}/edit', 'HomeController@edit');
Route::put('/posts/{post}', 'HomeController@update');
Route::delete('/posts/{post}', 'HomeController@destroy');
